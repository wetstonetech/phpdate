<?php 
class Date {
	
	//private Date type
	private $curDate;
	
	/*
	 * Constructor
	 * @param Int timestamp
	 * @return void
	 */
	function Date($time = null) {
		if($time) {
			$this->curDate = date('Y-m-d H:i:s', $time);
		}else{
			$this->curDate = date('Y-m-d H:i:s');
		}
		
	}
	
	/*
	 * 
	 * @return String
	 */
	function toString() {
		return strftime('%b %e, %Y', $this->getTime());
	}
	
	function __toString() {
		return $this->getMySQLDate();
	}
	
	function getHour() {
		return strftime('%I', $this->getTime());
	}
	function getHourOfDay() {
		return strftime('%H', $this->getTime());
	}
	function getMinute() {
		return strftime('%M', $this->getTime());
	}
	function getSecond() {
		return strftime('%S', $this->getTime());
	}
	function getYear() {
		return strftime('%Y', $this->getTime());
	}
	function getDayOfWeek() {
		return strftime('%u', $this->getTime());
	}
	function getDay() {
		return strftime('%d', $this->getTime());
	}
	function getMonth() {
		return strftime('%m', $this->getTime());
	}
	function getMeridiem() {
		return strftime('%P', $this->getTime());
	}
	function getDayName() {
		return strftime('%a', $this->getTime());
	}
	
	
	/*
	 * @param String format
	 * @return String date formatted
	 */
	function format($format = '%b %e, %Y') {
		return strftime($format, $this->getTime());
	}
	
	/*
	 * @param Date 
	 * @return Boolean
	 */
	function after(Date $date) {
	if($this->getTime() > $date->getTime()) {
			return true;
		}else{
			return false;
		}
	}
	
	/*
	 * @param Date
	 * @return Boolean
	 */
	function before(Date $date) {
	if($this->getTime() < $date->getTime()) {
			return true;
		}else{
			return false;
		}
	}
	
	/*
	 * @param Date
	 * @return Boolean
	 */
	function equals(Date $date) {
		if($this->getTime() == $date->getTime()) {
			return true;
		}else{
			return false;
		}
	}
	
	/*
	 * 
	 * @return Int
	 */
	function getTime() {
		return strtotime($this->curDate);
	}
	
	/*
	 * @param Int timestamp
	 * @return void
	 */
	function setTime($time) {
		$this->curDate = date('Y-m-d H:i:s', $time);
	}
	
	/*
	 * @param String date as string
	 * @return void
	 */
	function setDate($dateString) {
		$this->curDate = date('Y-m-d H:i:s', strtotime($dateString));
	}
	
	/*
	 * @param String timezone code/name
	 * @return void
	 */
	function setTimeZone($timezone) {
		$dateTimeZone = new DateTimeZone($timezone);
        $dateTime = new DateTime($this->curDate);
        $offset = $dateTimeZone->getOffset($dateTime);

        $this->setTime($this->getTime()+$offset);
		
	}
	
	/*
	 * @return String date
	 */
	function getMySQLDate() {
		return $this->curDate;
	}
	
	/*
	 * @return Int timestamp
	 */
	function getPHPDate() {
		return $this->getTime();
	}
	
	/*
	 * @param String type type of value being added to date
	 * @param String value number of $type being added
	 * @return void
	 */
	function add($type = 'DAY',$value = 0) {
		switch($type) {
			case 'SECOND': $this->setTime($this->getTime() + ($value));
			case 'MINUTE': $this->setTime($this->getTime() + (60 * $value));
			case 'HOUR': $this->setTime($this->getTime() + (3600 * $value));
			case 'DAY': $this->setTime($this->getTime() + (86400 * $value));
		}
	}
}
?>